package com.nba.frontEnd.testConsolidationAPI;

import com.nba.frontEnd.endpoints.base.BaseAPI;
import com.nba.frontEnd.endpoints.serviceObject.NBAContentAPIContent;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class contentAPIContentTest extends BaseAPI {

    NBAContentAPIContent content= new NBAContentAPIContent();
    @Test
    public void contentAPIContentFirstTest(){
        content.IsStatusCode200();
    }

    @Test
    public void contentAPIContentSecondTest(){
        Assert.assertTrue(content.checkStatusCode200());
    }

    @Test
    public void validateGetGameResponse()
    {
        Response response = given()
                .header(acceptHeader)
                .get(url+"private/1/game/");
        int statusCode = response.getStatusCode();
        ResponseBody body = response.getBody();

        System.out.println("Status code= "+statusCode);
        Assert.assertTrue(statusCode==200);
        System.out.println("Response Body is: " + body.asString());
    }
}

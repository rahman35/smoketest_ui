package com.nba.frontEnd.testConsolidationAPI;

import com.nba.frontEnd.endpoints.base.BaseAPI;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

public class contentAPIWriterTest extends BaseAPI {

    @Test
    public void validateGetWriterResponse()
    {
        Response response = given()
                .header(acceptHeader)
                .get(url+"private/1/writer/");
        int statusCode = response.getStatusCode();
        ResponseBody body = response.getBody();

        System.out.println("Status code= "+statusCode);
        Assert.assertTrue(statusCode==200);
        System.out.println("Response Body is: " + body.asString());
    }

}

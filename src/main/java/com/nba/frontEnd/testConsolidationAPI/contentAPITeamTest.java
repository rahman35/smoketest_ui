package com.nba.frontEnd.testConsolidationAPI;

import com.nba.frontEnd.endpoints.base.BaseAPI;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

public class contentAPITeamTest extends BaseAPI {

    @Test
    public void validateGetTeamResponse()
    {
        Response response = given()
                .header(acceptHeader)
                .get(url+"private/1/team/");
        int statusCode = response.getStatusCode();
        ResponseBody body = response.getBody();

        System.out.println("Status code= "+statusCode);
        Assert.assertTrue(statusCode==200);
        System.out.println("Response Body is: " + body.asString());
    }

}

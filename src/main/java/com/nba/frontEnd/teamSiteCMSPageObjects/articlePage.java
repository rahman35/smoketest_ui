package com.nba.frontEnd.teamSiteCMSPageObjects;

import com.nba.frontEnd.base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class articlePage extends BasePage {

    @FindBy(xpath = "//div[contains(text(),'Articles')]") WebElement articleBtn;
    @FindBy(linkText = "Add New Article") WebElement addNewArticleBtn;
    @FindBy(xpath = "//textarea[@id='post-title-0']") WebElement addTittle;
    @FindBy(xpath = "//button[contains(text(),'Publish')]") WebElement publishBtn;
    @FindBy(xpath = "//body/div[@id='wpwrap']/div[@id='wpcontent']/div[@id='wpbody']/div[@id='wpbody-content']/div[2]/div[1]/div[1]/div[1]/div[2]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/button[1]")WebElement confirmPublication;


    public articlePage(WebDriver driver) { super(driver); }

    {
        driver.get("https://manage-teams-qa.nba.com/wp-login.php");
        driver.findElement(By.xpath("//a[contains(text(),'SSO Login (NBA accounts)')]")).click();
    }

    public boolean articleBtnClick() {articleBtn.click();return true;}
    public boolean enterData(String text) throws InterruptedException {
        addTittle.sendKeys(text);
        return true;
    }
    public boolean publishBtnClick(){publishBtn.click();return true;}
    public boolean publishConfirmation(){confirmPublication.click();return true;}

}


package com.nba.frontEnd.teamSiteCMSPageObjects;

import com.nba.frontEnd.base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class galleriePage extends BasePage {

    @FindBy(xpath = "//div[contains(text(),'Galleries')]") WebElement galleriesBtn;
    @FindBy(linkText = "Add New Gallery") WebElement addNewGalleryBtn;
    @FindBy(xpath = "//textarea[@id='post-title-0']") WebElement addTittle;
    @FindBy(xpath = "//button[contains(text(),'Publish')]") WebElement publishBtn;
    @FindBy(xpath = "//body/div[@id='wpwrap']/div[@id='wpcontent']/div[@id='wpbody']/div[@id='wpbody-content']/div[2]/div[1]/div[1]/div[1]/div[2]/div[4]/div[2]/div[1]/div[1]/div[1]/div[1]/button[1]")WebElement confirmPublication;
    @FindBy(id = "//input[@id='cb-select-1000250']")WebElement firstCheckBox;
    @FindBy(xpath = "//select[@id='bulk-action-selector-bottom']")WebElement bulkSelectValue;
    @FindBy(id = "doaction2")WebElement applyBtn;
    @FindBy(linkText = "Undo")WebElement undoBtn;

    public galleriePage(WebDriver driver) {super(driver);}
    {
        driver.get("https://manage-teams-qa.nba.com/wp-login.php");
        driver.manage().window().maximize();
        driver.findElement(By.xpath("//a[contains(text(),'SSO Login (NBA accounts)')]")).click();
    }


    public boolean galleryBtnClick() {galleriesBtn.click();return true;}
    public boolean addEvent(){addNewGalleryBtn.click();return true;}

    public boolean enterData(String text) throws InterruptedException {
        addTittle.sendKeys(text);
        return true;
    }

    public boolean clickBulkAction()
    {
        Select s= new Select(bulkSelectValue);
        s.selectByIndex(2);
        return true;
    }

    public boolean publishBtnClick(){publishBtn.click();return true;}
    public boolean publishConfirmation(){confirmPublication.click();return true;}
    public boolean clickCheckbox(){firstCheckBox.click();return true;}
    public boolean clickApplyBtn(){applyBtn.click();return true;}
    public boolean undoBtnVisibility(){undoBtn.isDisplayed();return true;}

}


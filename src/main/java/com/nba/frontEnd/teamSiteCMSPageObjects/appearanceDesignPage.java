package com.nba.frontEnd.teamSiteCMSPageObjects;

import com.nba.frontEnd.base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class appearanceDesignPage extends BasePage {

    // colors page UI elements
    @FindBy(xpath = "//div[contains(text(),'Appearance')]") WebElement appearanceBtn;
    @FindBy(linkText = "Design") WebElement menusBtn;
    @FindBy(xpath = "//input[@id='inspector-text-control-0']")WebElement primaryDarkBox;
    @FindBy(xpath = "//input[@id='inspector-text-control-1']")WebElement secondaryDarkBox;
    @FindBy(xpath = "//input[@id='inspector-text-control-2']")WebElement primaryLightBox;
    @FindBy(xpath = "//input[@id='inspector-text-control-3']")WebElement secondaryLightBox;
    @FindBy(xpath = "//input[@id='inspector-text-control-4']")WebElement primaryAccentBox;
    @FindBy(xpath = "//input[@id='inspector-text-control-5']")WebElement secondaryAccentBox;

    // fonts page UI elements
    @FindBy(xpath = "//button[contains(text(),'Fonts')]") WebElement FontsTab;
    @FindBy(xpath = "//input[@id='inspector-text-control-6']")WebElement kitIDBox;
    @FindBy(xpath = "//input[@id='inspector-text-control-7']")WebElement googleFontsCSSBox;
    @FindBy(xpath = "//input[@id='inspector-text-control-8']")WebElement brandFontsBox;
    @FindBy(xpath = "//input[@id='inspector-text-control-9']")WebElement contentFontBox;
    @FindBy(xpath = "//input[@id='inspector-text-control-10']")WebElement numberFontBox;

    // Images page UI elements
    @FindBy(xpath = "//button[contains(text(),'Images')]") WebElement imagesTab;
    @FindBy(xpath = "//body/div[@id='wpwrap']/div[@id='wpcontent']/div[@id='wpbody']/div[@id='wpbody-content']/div[@id='nba-teams-theme-settings-page']/main[1]/form[1]/div[1]/section[1]/div[1]/div[1]/div[1]/div[2]/img[1]")WebElement image1;
    @FindBy(xpath = "//body/div[@id='wpwrap']/div[@id='wpcontent']/div[@id='wpbody']/div[@id='wpbody-content']/div[@id='nba-teams-theme-settings-page']/main[1]/form[1]/div[1]/section[1]/div[1]/div[2]/div[1]/div[2]/img[1]")WebElement image2;
    @FindBy(xpath = "//input[@id='root_logo']")WebElement logoUploadBox;
    @FindBy(xpath = "//input[@id='root_siteIcon']")WebElement siteIconUploadBox;



    public appearanceDesignPage(WebDriver driver) {super(driver);}
    {
        driver.get("https://manage-teams-qa.nba.com/wp-login.php");
        driver.manage().window().maximize();
        driver.findElement(By.xpath("//a[contains(text(),'SSO Login (NBA accounts)')]")).click();
    }

    //Colors tab
    public boolean appearanceBtnClick() {appearanceBtn.click();return true;}
    public boolean designPageClick(){menusBtn.click();return true;}
    public boolean getPrimaryDarkText(){primaryDarkBox.isDisplayed();return true;}
    public boolean getSecondaryDarkText(){secondaryDarkBox.isDisplayed();return true;}
    public boolean getPrimaryLightText(){primaryLightBox.isDisplayed();return true;}
    public boolean getSecondaryLightText(){secondaryLightBox.isDisplayed();return true;}
    public boolean getPrimaryAccentBoxText(){primaryAccentBox.isDisplayed();return true;}
    public boolean getSecondaryAccentBoxText(){secondaryAccentBox.isDisplayed();return true;}

    //Fonts tab
    public boolean fontsTabClick() {FontsTab.click();return true;}
    public boolean getKitIDBoxText(){kitIDBox.isDisplayed();return true;}
    public boolean getGoogleFontsCSSBoxText(){googleFontsCSSBox.isDisplayed();return true;}
    public boolean getBrandFontsBoxText(){brandFontsBox.isDisplayed();return true;}
    public boolean getContentFontBoxText(){contentFontBox.isDisplayed();return true;}
    public boolean getNumberFontBoxText(){numberFontBox.isDisplayed();return true;}

    //Images tab
    public boolean imagesTabClick() {imagesTab.click();return true;}
    public boolean getFirstLogoVisibility(){image1.isDisplayed();return true;}
    public boolean getSecondLogoVisibility(){image2.isDisplayed();return true;}
    public boolean getLogoUploadBoxVisibility(){logoUploadBox.isDisplayed();return true;}
    public boolean getSiteIconLogoVisibility(){siteIconUploadBox.isDisplayed();return true;}



}


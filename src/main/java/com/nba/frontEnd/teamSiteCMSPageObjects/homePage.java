package com.nba.frontEnd.teamSiteCMSPageObjects;

import com.nba.frontEnd.base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;




public class homePage extends BasePage {

    @FindBy (xpath = "//div[contains(text(),'Media')]") WebElement mediaBtn;
    @FindBy (xpath = "//div[contains(text(),'Pages')]") WebElement pagesBtn;
    @FindBy (xpath = "//div[contains(text(),'Articles')]") WebElement articlesBtn;
    @FindBy (xpath = "//div[contains(text(),'Collections')]") WebElement collectionBtn;
    @FindBy (xpath = "//div[contains(text(),'Events')]") WebElement eventsBtn;
    @FindBy (xpath = "//div[contains(text(),'Galleries')]") WebElement galleriesBtn;
    @FindBy (xpath = "//div[contains(text(),'Games')]") WebElement gamesBtn;
    @FindBy (xpath = "//div[contains(text(),'Videos')]") WebElement videosBtn;
    @FindBy (xpath = "//div[contains(text(),'Appearance')]") WebElement appearanceBtn;
    @FindBy (xpath = "//div[contains(text(),'Users')]") WebElement usersBtn;
    @FindBy (xpath = "//div[contains(text(),'Settings')]") WebElement settingsBtn;

    public homePage(WebDriver driver) { super(driver); }
    {
        driver.get("https://manage-teams-qa.nba.com/wp-login.php");
        driver.findElement(By.xpath("//a[contains(text(),'SSO Login (NBA accounts)')]")).click();
        driver.manage().window().maximize();
    }

    public Boolean mediaBtnVisibility(){return mediaBtn.isDisplayed();}
    public Boolean pagesBtnVisibility(){return pagesBtn.isDisplayed();}
    public Boolean articlesBtnVisibility(){return articlesBtn.isDisplayed();}
    public Boolean collectionBtnVisibility(){return collectionBtn.isDisplayed();}
    public Boolean eventsBtnVisibility(){return eventsBtn.isDisplayed();}
    public Boolean galleriesBtnVisibility(){return galleriesBtn.isDisplayed();}
    public Boolean gamesBtnVisibility(){return gamesBtn.isDisplayed();}
    public Boolean videosBtnVisibility(){return videosBtn.isDisplayed();}
    public Boolean appearanceBtnVisibility(){return appearanceBtn.isDisplayed();}
    public Boolean usersBtnVisibility(){return usersBtn.isDisplayed();}
    public Boolean settingsBtnVisibility(){return settingsBtn.isDisplayed();}
}

package com.nba.frontEnd.teamSiteCMSPageObjects;

import com.nba.frontEnd.base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class eventsPage extends BasePage {

    @FindBy(xpath = "//div[contains(text(),'Events')]") WebElement eventsBtn;
    @FindBy(xpath = "//body/div[@id='wpwrap']/div[@id='wpcontent']/div[@id='wpbody']/div[@id='wpbody-content']/div[3]/a[1]") WebElement addNewEventBtn;
    @FindBy(xpath = "//input[@id='inspector-text-control-0']") WebElement nameField;
    @FindBy(xpath = "//button[contains(text(),'Publish')]") WebElement publishBtn;
    @FindBy(partialLinkText = "Publish")WebElement postVerification;
    @FindBy(xpath = "//input[@id='cb-select-all-1']")WebElement selectAllCheckbox;
    @FindBy(xpath = "//select[@id='bulk-action-selector-bottom']")WebElement bulkSelectValue;
    @FindBy(id = "doaction2")WebElement applyBtn;
    @FindBy(linkText = "Undo")WebElement undoBtn;


    public eventsPage(WebDriver driver) {super(driver);}
    {
        driver.get("https://manage-teams-qa.nba.com/wp-login.php");
        driver.manage().window().maximize();
        driver.findElement(By.xpath("//a[contains(text(),'SSO Login (NBA accounts)')]")).click();
    }


    public boolean eventsBtnClick() {eventsBtn.click();return true;}
    public boolean addEvent(){addNewEventBtn.click();return true;}

    public boolean enterData(String text) throws InterruptedException {
        nameField.sendKeys(text);
        return true;
    }

    public boolean publishBtnClick(){publishBtn.click();return true;}
    public boolean getPostVerification(){return postVerification.isDisplayed();}
    public boolean getSelectAll(){selectAllCheckbox.click();return true;}

    public boolean clickBulkAction()
    {
        Select s= new Select(bulkSelectValue);
        s.selectByIndex(2);
        return true;
    }

    public boolean clickApplyBtn(){applyBtn.click();return true;}
    public boolean undoBtnVisibility(){undoBtn.isDisplayed();return true;}

}


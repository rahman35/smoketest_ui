package com.nba.frontEnd.testTeamSiteCMS;

import com.nba.frontEnd.base.BaseTest;
import com.nba.frontEnd.teamSiteCMSPageObjects.appearanceDesignPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class teamSiteCMSAppearanceDesignColorTest extends BaseTest {

    @Test
    public void validateAppearanceColors()
    {
        appearanceDesignPage colorsPage= new appearanceDesignPage(getDriver());
        Assert.assertTrue(colorsPage.appearanceBtnClick());
        Assert.assertTrue(getDriver().getTitle().contains("Themes"));
        Assert.assertTrue(colorsPage.designPageClick());

        // validate design color textboxes
        Assert.assertTrue(colorsPage.getPrimaryDarkText());
        Assert.assertTrue(colorsPage.getSecondaryDarkText());
        Assert.assertTrue(colorsPage.getPrimaryLightText());
        Assert.assertTrue(colorsPage.getSecondaryLightText());
        Assert.assertTrue(colorsPage.getPrimaryAccentBoxText());
        Assert.assertTrue(colorsPage.getSecondaryAccentBoxText());
    }

}

package com.nba.frontEnd.testTeamSiteCMS;

import com.nba.frontEnd.base.BaseTest;
import com.nba.frontEnd.teamSiteCMSPageObjects.pageCreationPage;
import org.testng.Assert;
import org.testng.annotations.Test;


public class teamSiteCMSPageTest extends BaseTest {


    @Test(priority = 1)
    public void validateCMSPageFunctionality() throws InterruptedException {
        pageCreationPage pcp= new pageCreationPage(getDriver());
        Assert.assertTrue(pcp.galleryBtnClick());
        Assert.assertTrue(getDriver().getCurrentUrl().contains("page"));
        Assert.assertTrue(getDriver().getTitle().contains("Pages"));
        System.out.println(getDriver().getTitle());
        Assert.assertTrue(pcp.addEvent());
        pcp.enterData("test"+java.time.LocalTime.now());
        Assert.assertTrue(pcp.publishBtnClick());
        Assert.assertTrue(pcp.publishConfirmation());
        Thread.sleep(4000);
        getDriver().navigate().back();
        Thread.sleep(2000);
        getDriver().navigate().refresh();
    }

}

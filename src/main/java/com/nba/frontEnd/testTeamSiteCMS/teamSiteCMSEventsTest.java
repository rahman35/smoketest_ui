package com.nba.frontEnd.testTeamSiteCMS;

import com.nba.frontEnd.base.BaseTest;
import com.nba.frontEnd.teamSiteCMSPageObjects.eventsPage;
import org.testng.Assert;
import org.testng.annotations.Test;


public class teamSiteCMSEventsTest extends BaseTest {


    @Test(priority = 1)
    public void validateCMSEventFunctionality() throws InterruptedException {
        eventsPage ep= new eventsPage(getDriver());
        Assert.assertTrue(ep.eventsBtnClick());
        Assert.assertTrue(getDriver().getCurrentUrl().contains("events"));
        Assert.assertTrue(getDriver().getTitle().contains("Events"));
        System.out.println(getDriver().getTitle());
        Assert.assertTrue(ep.addEvent());
        ep.enterData("test"+java.time.LocalTime.now());
        Assert.assertTrue(ep.publishBtnClick());
        Thread.sleep(3000);
        Assert.assertTrue(ep.eventsBtnClick());
        Assert.assertTrue(ep.getPostVerification());
        Assert.assertTrue(ep.getSelectAll());
        Assert.assertTrue(ep.clickBulkAction());
        Assert.assertTrue(ep.clickApplyBtn());
        Assert.assertTrue(ep.undoBtnVisibility());
    }
}

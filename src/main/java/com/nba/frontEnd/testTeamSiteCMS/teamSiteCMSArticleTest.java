package com.nba.frontEnd.testTeamSiteCMS;

import com.nba.frontEnd.base.BaseTest;
import com.nba.frontEnd.teamSiteCMSPageObjects.articlePage;
import org.testng.Assert;
import org.testng.annotations.Test;


public class teamSiteCMSArticleTest extends BaseTest {

    @Test
    public void validateCMSArticle()
    {
        articlePage ap= new articlePage(getDriver());
        Assert.assertTrue(ap.articleBtnClick());
        Assert.assertTrue(getDriver().getCurrentUrl().contains("article"));
        Assert.assertTrue(getDriver().getTitle().contains("Articles"));
        System.out.println(getDriver().getTitle());
    }


}

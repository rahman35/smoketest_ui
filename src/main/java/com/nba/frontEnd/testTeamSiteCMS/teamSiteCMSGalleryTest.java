package com.nba.frontEnd.testTeamSiteCMS;

import com.nba.frontEnd.base.BaseTest;
import com.nba.frontEnd.teamSiteCMSPageObjects.galleriePage;
import org.testng.Assert;
import org.testng.annotations.Test;


public class teamSiteCMSGalleryTest extends BaseTest {


    @Test(priority = 1)
    public void validateCMSGalleryFunctionality() throws InterruptedException {
        galleriePage gp= new galleriePage(getDriver());
        Assert.assertTrue(gp.galleryBtnClick());
        Assert.assertTrue(getDriver().getCurrentUrl().contains("gallery"));
        Assert.assertTrue(getDriver().getTitle().contains("Galleries"));
        System.out.println(getDriver().getTitle());
        Assert.assertTrue(gp.addEvent());
        gp.enterData("test"+java.time.LocalTime.now());
        Assert.assertTrue(gp.publishBtnClick());
        Assert.assertTrue(gp.publishConfirmation());
        Thread.sleep(4000);
        getDriver().navigate().back();
        getDriver().navigate().refresh();
    }

}

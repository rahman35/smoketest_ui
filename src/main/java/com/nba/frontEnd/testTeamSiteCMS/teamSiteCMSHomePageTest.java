package com.nba.frontEnd.testTeamSiteCMS;

import com.nba.frontEnd.base.BaseTest;
import com.nba.frontEnd.teamSiteCMSPageObjects.homePage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class teamSiteCMSHomePageTest extends BaseTest {

    @Test
    public void validateCMSHomePage()
    {
        homePage hp= new homePage(getDriver());
        Assert.assertTrue(hp.mediaBtnVisibility());
        Assert.assertTrue(hp.pagesBtnVisibility());
        Assert.assertTrue(hp.articlesBtnVisibility());
        Assert.assertTrue(hp.collectionBtnVisibility());
        Assert.assertTrue(hp.eventsBtnVisibility());
        Assert.assertTrue(hp.galleriesBtnVisibility());
        Assert.assertTrue(hp.gamesBtnVisibility());
        Assert.assertTrue(hp.videosBtnVisibility());
        Assert.assertTrue(hp.appearanceBtnVisibility());
        Assert.assertTrue(hp.usersBtnVisibility());
        Assert.assertTrue(hp.settingsBtnVisibility());
        Assert.assertTrue(getDriver().getTitle().contains("Dashboard"));
        System.out.println(getDriver().getTitle());

    }
}

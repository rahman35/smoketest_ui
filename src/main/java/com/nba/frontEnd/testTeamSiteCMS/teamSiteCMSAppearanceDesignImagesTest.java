package com.nba.frontEnd.testTeamSiteCMS;

import com.nba.frontEnd.base.BaseTest;
import com.nba.frontEnd.teamSiteCMSPageObjects.appearanceDesignPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class teamSiteCMSAppearanceDesignImagesTest extends BaseTest {

    @Test
    public void validateAppearanceFonts()
    {
        appearanceDesignPage iamgesPage= new appearanceDesignPage(getDriver());
        Assert.assertTrue(iamgesPage.appearanceBtnClick());
        Assert.assertTrue(getDriver().getTitle().contains("Themes"));
        Assert.assertTrue(iamgesPage.designPageClick());
        Assert.assertTrue(iamgesPage.fontsTabClick());

        Assert.assertTrue(iamgesPage.getKitIDBoxText());
        Assert.assertTrue(iamgesPage.getGoogleFontsCSSBoxText());
        Assert.assertTrue(iamgesPage.getBrandFontsBoxText());
        Assert.assertTrue(iamgesPage.getContentFontBoxText());
        Assert.assertTrue(iamgesPage.getNumberFontBoxText());
    }
}

package com.nba.frontEnd.testTeamSiteCMS;

import com.nba.frontEnd.base.BaseTest;
import com.nba.frontEnd.teamSiteCMSPageObjects.appearanceDesignPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class teamSiteCMSAppearanceDesignFontsTest extends BaseTest {

    @Test
    public void validateAppearanceFonts()
    {
        appearanceDesignPage fontsPage= new appearanceDesignPage(getDriver());
        Assert.assertTrue(fontsPage.appearanceBtnClick());
        Assert.assertTrue(getDriver().getTitle().contains("Themes"));
        Assert.assertTrue(fontsPage.designPageClick());
        Assert.assertTrue(fontsPage.imagesTabClick());

        Assert.assertTrue(fontsPage.getFirstLogoVisibility());
        Assert.assertTrue(fontsPage.getSecondLogoVisibility());
        Assert.assertTrue(fontsPage.getLogoUploadBoxVisibility());
        Assert.assertTrue(fontsPage.getSiteIconLogoVisibility());
    }
}

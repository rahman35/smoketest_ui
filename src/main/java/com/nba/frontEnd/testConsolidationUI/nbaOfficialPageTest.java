package com.nba.frontEnd.testConsolidationUI;

import com.nba.frontEnd.base.BaseTest;
import com.nba.frontEnd.pageObject.nbaOfficialPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class nbaOfficialPageTest extends BaseTest {

    @Test
    public void validateOfficialUIElements()
    {
        nbaOfficialPage officialPage= new nbaOfficialPage(getDriver());
        Assert.assertTrue(getDriver().getCurrentUrl().contains("official"));
        Assert.assertTrue(officialPage.ruleBookVisibilitiy());
        Assert.assertTrue(officialPage.pointsofEduVisibilitiy());
        Assert.assertTrue(officialPage.nbaArchiveVisibilitiy());

    }
}

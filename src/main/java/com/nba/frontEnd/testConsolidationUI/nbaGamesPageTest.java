package com.nba.frontEnd.testConsolidationUI;

import com.nba.frontEnd.base.BaseTest;
import com.nba.frontEnd.pageObject.nbaGamePage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class nbaGamesPageTest extends BaseTest {

    @Test
    public void validateGamePageUIElements()
    {
        nbaGamePage gamesPage = new nbaGamePage(getDriver());
        Assert.assertTrue(getDriver().getCurrentUrl().contains("games"));
        Assert.assertTrue(gamesPage.navBarVisibility());
        Assert.assertTrue(gamesPage.headlinesVisibility());
    }


}

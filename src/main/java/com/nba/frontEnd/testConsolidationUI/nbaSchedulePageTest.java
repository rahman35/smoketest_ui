package com.nba.frontEnd.testConsolidationUI;

import com.nba.frontEnd.base.BaseTest;
import com.nba.frontEnd.pageObject.nbaSchedulePage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class nbaSchedulePageTest extends BaseTest {
    
    @Test(priority=3)
    public void validateSchedulePage()
    {
        nbaSchedulePage schedulepage = new nbaSchedulePage(getDriver());
        Assert.assertTrue(getDriver().getCurrentUrl().contains("schedule"));
        System.out.println(getDriver().getTitle());

    }

}

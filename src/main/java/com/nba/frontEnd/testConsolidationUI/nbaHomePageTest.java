package com.nba.frontEnd.testConsolidationUI;

import com.nba.frontEnd.base.BaseTest;
import com.nba.frontEnd.pageObject.nbaHomePage;
import org.testng.Assert;
import org.testng.annotations.Test;


public class nbaHomePageTest extends BaseTest {

    @Test(priority=1)
    public void validateHomePageLoginTests(){
        nbaHomePage nbaPage = new nbaHomePage(getDriver());

        Assert.assertTrue(nbaPage
                .clickOnSignInBtn()
                .clickOnSignInToNBAAccount()
                .setUserAndPassword("rrosario@nba.com", "13301330").NBANavVisibility());
    }
    
    @Test(priority=2)
    public void validateLogoDisplayedTest()
    {
        nbaHomePage hp= new nbaHomePage(getDriver());
        Assert.assertTrue(hp.NBALogoVisibility());
    }
}

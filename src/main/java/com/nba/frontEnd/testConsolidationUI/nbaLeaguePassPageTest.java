package com.nba.frontEnd.testConsolidationUI;

import com.nba.frontEnd.base.BaseTest;
import com.nba.frontEnd.pageObject.nbaLeaguePassPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class nbaLeaguePassPageTest extends BaseTest {

    @Test
    public void validateLeaguePassPage()
    {
        nbaLeaguePassPage lp= new nbaLeaguePassPage(getDriver());
        lp.acceptCookies();
        Assert.assertTrue(getDriver().getCurrentUrl().contains("streaming-subscriptions"));
        Assert.assertTrue(lp.boldTextVisibility());
        Assert.assertTrue(lp.getStartedVisibility());
    }
}

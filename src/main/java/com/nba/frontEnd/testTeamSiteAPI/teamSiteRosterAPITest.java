package com.nba.frontEnd.testTeamSiteAPI;

import com.nba.frontEnd.endpoints.base.BaseAPI;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class teamSiteRosterAPITest extends BaseAPI {

    @Test
    public void validateGetTeamSiteRosterAPI()
    {
        Response response = given()
                .header(acceptHeader)
                .get(url+"public/1/teams/1610612741/roster");
        int statusCode = response.getStatusCode();
        ResponseBody body = response.getBody();

        System.out.println("Status code= "+statusCode);
        Assert.assertTrue(statusCode==200);
        System.out.println("Response Body is: " + body.asString());
    }

    @Test
    public void validateGetTeamSiteContentArticleAPI()
    {
        Response response =given()
                .header(acceptHeader)
                .get(url+"public/1/teams/1610612741/content/article");
        int statusCode = response.getStatusCode();
        ResponseBody body = response.getBody();

        System.out.println("Status code= "+statusCode);
        Assert.assertTrue(statusCode==200);
        System.out.println("Response Body is: " + body.asString());
    }

    @Test
    public void validateGetTeamSiteContentIndexAPI()
    {
        Response response =given()
                .header(acceptHeader)
                .get(url+"public/1/teams/1610612741/content/index");
        int statusCode = response.getStatusCode();
        ResponseBody body = response.getBody();

        System.out.println("Status code= "+statusCode);
        Assert.assertTrue(statusCode==200);
        System.out.println("Response Body is: " + body.asString());
    }

    @Test
    public void validateGetTeamSiteTeamLeadersAPI()
    {
        Response response =given()
                .header(acceptHeader)
                .get(url+"public/1/teams/1610612741/leaders?season=2020-21");
        int statusCode = response.getStatusCode();
        ResponseBody body = response.getBody();

        System.out.println("Status code= "+statusCode);
        Assert.assertTrue(statusCode==200);
        System.out.println("Response Body is: " + body.asString());
    }

    @Test
    public void validateGetTeamSiteTeamScheduleAPI()
    {
        Response response =given()
                .header(acceptHeader)
                .get(url+"private/1/teams/1610612741/schedule");
        int statusCode = response.getStatusCode();
        ResponseBody body = response.getBody();

        System.out.println("Status code= "+statusCode);
        Assert.assertTrue(statusCode==200);
        System.out.println("Response Body is: " + body.asString());
    }


}


package com.nba.frontEnd.endpoints.serviceObject;

import com.nba.frontEnd.endpoints.base.BaseAPI;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import static io.restassured.RestAssured.given;

public class NBAContentAPIHealthCheck {

    public ValidatableResponse IsStatusCode200(){
        System.out.println("contentAPIHealthCheck");
        return given().when().get("/public/1/health-check").then().statusCode(200);
    }

    public ValidatableResponse IsNotStatusCode200(){
        return given().when().get("/public/1/health-check").then().statusCode(300);
    }

    public boolean checkStatusCode200(){
        RequestSpecification httpRequest = RestAssured.given();
        Response response = httpRequest.get("/public/1/health-check");
        int statusCode = response.getStatusCode();
        return statusCode == 200 ? true : false;
    }

}

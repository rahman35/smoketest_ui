package com.nba.frontEnd.endpoints.serviceObject;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class NBAContentAPISiteLayout {

    public ValidatableResponse IsStatusCode200(){
        System.out.println("contentAPIContent");
        return given().when().get("/public/1/site/layout/nav").then().statusCode(200);
    }

    public ValidatableResponse IsNotStatusCode200(){
        return given().when().get("/public/1/site/layout/nav").then().statusCode(300);
    }

    public boolean checkStatusCode200(){
        RequestSpecification httpRequest = RestAssured.given();
        Response response = httpRequest.get("/public/1/site/layout/nav");
        int statusCode = response.getStatusCode();
        return statusCode == 200 ? true : false;
    }

}

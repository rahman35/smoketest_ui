package com.nba.frontEnd.endpoints.base;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import org.testng.annotations.BeforeClass;



public class BaseAPI {
 public Header acceptHeader = new Header("x-api-key", "nLraYn2eCT448xcHLHRag6mooD9sjEJf3GDZF8oB");
 public String url= "https://content-api-qa.nba.com/";

    @BeforeClass
    public void  beforeClass() {

        System.out.println("beforeClass");
        RestAssured.baseURI = "https://content-api-qa.nba.com";

    }

}

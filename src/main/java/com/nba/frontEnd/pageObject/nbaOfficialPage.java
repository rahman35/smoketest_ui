package com.nba.frontEnd.pageObject;

import com.nba.frontEnd.base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class nbaOfficialPage extends BasePage {

    @FindBy(xpath = "//h5[contains(text(),'Search the NBA Rulebook')]") WebElement nbaRuleBook;
    @FindBy(xpath = "//h5[contains(text(),'Points of Education')]") WebElement nbaPointsofEdu;
    @FindBy(xpath = "//h5[contains(text(),'NBA Replay Archive')]") WebElement nbaArchive;


    public nbaOfficialPage(WebDriver driver) { super(driver); }
    {
        driver.get("https://official.nba.com/");
    }

    public Boolean ruleBookVisibilitiy() {return nbaRuleBook.isDisplayed();}
    public Boolean pointsofEduVisibilitiy() {return nbaPointsofEdu.isDisplayed();}
    public Boolean nbaArchiveVisibilitiy() {return nbaArchive.isDisplayed();}



}

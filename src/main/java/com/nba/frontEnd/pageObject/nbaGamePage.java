package com.nba.frontEnd.pageObject;

import com.nba.frontEnd.base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class nbaGamePage extends BasePage {

    @FindBy(xpath = "//body/div[@id='__next']/div[2]/div[3]/div[1]/div[1]/section[1]/div[1]") WebElement navBar;
    @FindBy(xpath = "//h2[contains(text(),'Headlines')]")WebElement headlinesSection;

    public nbaGamePage(WebDriver driver) { super(driver); }

    {
        driver.get("https://www.nba.com/games");
    }

    public Boolean navBarVisibility (){return navBar.isDisplayed();}
    public Boolean headlinesVisibility (){return headlinesSection.isDisplayed();}

}

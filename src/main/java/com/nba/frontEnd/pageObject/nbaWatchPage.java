package com.nba.frontEnd.pageObject;

import com.nba.frontEnd.base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class nbaWatchPage extends BasePage {

    @FindBy(xpath = "//body/div[@id='root']/div[@id='pFeatured']/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]")WebElement playVid;

    public nbaWatchPage(WebDriver driver) { super(driver); }
    {
        driver.get("https://www.nba.com/watch/");
    }
    public Boolean vidClick() { return playVid.isDisplayed();}}




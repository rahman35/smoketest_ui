package com.nba.frontEnd.pageObject;

import com.nba.frontEnd.base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class nbaLeaguePassPage extends BasePage {

    @FindBy(xpath = "//body/div[11]/iframe[1]")WebElement bannerClick;
    @FindBy(xpath = "//h1[contains(text(),'Watch the NBA, your way')]") WebElement boldText;
    @FindBy(xpath = "//body/div[@id='nbaRenderContainer']/div[1]/section[1]/section[1]/div[3]/a[1]")WebElement getStarted;
    @FindBy(linkText = "GET STARTED")WebElement accept;

    public nbaLeaguePassPage(WebDriver driver) { super(driver); }
    {
        driver.get("https://watch.nba.com/streaming-subscriptions");
    }

    public void acceptCookies() {accept.click();}
    public Boolean boldTextVisibility() {return  boldText.isDisplayed();}
    public Boolean getStartedVisibility() {return  getStarted.isDisplayed();}

}

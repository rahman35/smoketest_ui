package com.nba.frontEnd.pageObject;

import com.nba.frontEnd.base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class nbaSchedulePage  extends BasePage {


    public nbaSchedulePage(WebDriver driver) { super(driver); }

    {
        driver.get("https://www.nba.com/schedule");
        driver.manage().window().maximize();
    }


}

package com.nba.frontEnd.testTeamSiteUI;

import com.nba.frontEnd.base.BaseTest;
import com.nba.frontEnd.teamSitePageObjects.teamSiteRosterPage;
import org.testng.Assert;
import org.testng.annotations.Test;


public class teamSitePlayersMerchandiseTest extends BaseTest {

    @Test
    public void validateMerchandisePage()
    {
        teamSiteRosterPage rsp= new teamSiteRosterPage(getDriver());
        Assert.assertTrue(getDriver().getCurrentUrl().contains("/roster"));
        Assert.assertTrue(rsp.clickFirstPlayer());
        rsp.shopPlayersMerchClick();
    }



}

package com.nba.frontEnd.testTeamSiteUI;

import com.nba.frontEnd.base.BaseTest;
import com.nba.frontEnd.teamSitePageObjects.teamSiteRosterPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class teamSiteRosterPagesUITest extends BaseTest {


    @Test
    public void validateTeamRosterUIElements() throws InterruptedException {

        teamSiteRosterPage rp= new teamSiteRosterPage(getDriver());
        Assert.assertTrue(getDriver().getCurrentUrl().contains("/roster"));
        Assert.assertTrue(rp.getPrimaryNav());
        Assert.assertTrue(rp.getFirstPLayer());
        Assert.assertTrue(rp.clickFirstPlayer());
        Assert.assertTrue(rp.seasonVisibility());
        Assert.assertTrue(rp.careerVisibility());
        Assert.assertTrue(rp.heightVisibility());
        Assert.assertTrue(rp.weightVisibility());
        Assert.assertTrue(rp.ageVisibility());
        Assert.assertTrue(rp.yearsProVisibility());
        Assert.assertTrue(rp.countryVisibility());
        Assert.assertTrue(rp.headCoachVisibility());
    }
}

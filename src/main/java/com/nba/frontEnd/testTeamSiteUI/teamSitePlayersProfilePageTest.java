package com.nba.frontEnd.testTeamSiteUI;

import com.nba.frontEnd.base.BaseTest;
import com.nba.frontEnd.teamSitePageObjects.teamSitePlayersStatsPage;
import com.nba.frontEnd.teamSitePageObjects.teamSiteRosterPage;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.Iterator;
import java.util.Set;

public class teamSitePlayersProfilePageTest extends BaseTest {

    @Test
    public void validatePlayersProfilePage()
    {
        //landing Page
        teamSiteRosterPage tsrp= new teamSiteRosterPage(getDriver());
        Assert.assertTrue(getDriver().getCurrentUrl().contains("/roster"));
        Assert.assertTrue(tsrp.clickFirstPlayer());
        tsrp.playersBioClick();

        Set<String> windowsIds= getDriver().getWindowHandles();
        Iterator<String> iter=windowsIds.iterator();
        String parentWindow= iter.next();
        String childWindow= iter.next();

        //switchingToNextWindow
        getDriver().switchTo().window(childWindow);
        teamSitePlayersStatsPage statsPage = new teamSitePlayersStatsPage(getDriver());
        Assert.assertTrue(getDriver().getCurrentUrl().contains("stats/player"));
        Assert.assertTrue(statsPage.headshotVisibility());
      //  Assert.assertTrue(statsPage.seasonTypeVisibility());
        Assert.assertTrue(statsPage.perModeVisibility());
    }
}

package com.nba.frontEnd.teamSitePageObjects;

import com.nba.frontEnd.base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class teamSitePlayersMerchandisePage extends BasePage {

    @FindBy (xpath = "//body/div[@id='__next']/main[1]/article[1]/div[1]/div[1]/a[1]/div[1]/div[2]/div[1]/img[1]")WebElement firstPlayer;
    @FindBy (xpath = "//header/div[3]/div[1]/a[1]/img[1]")WebElement nbaStoreLogo;
    @FindBy (xpath = "//input[@id='typeahead-input-desktop']")WebElement searchBox;
    @FindBy (xpath = "//body/div[3]/div[1]/div[8]/div[1]/div[1]")WebElement leftNavBar;

    public teamSitePlayersMerchandisePage(WebDriver driver) { super(driver); }
    {
        driver.get(coprDev+"www-qa.nba.com/bulls/roster");
        driver.manage().window().maximize();
        driver.findElement(By.id("username")).sendKeys("nbaDev");
        driver.findElement(By.id("password")).sendKeys("Basketba11");
        driver.findElement(By.xpath("//button[contains(text(),'Login')]")).click();
    }

    public boolean clickFirstPlayer() {firstPlayer.click();return true; }
    public Boolean storeLogoVisibility() {return nbaStoreLogo.isDisplayed();}
    public Boolean searchBoxVisibility() {return searchBox.isDisplayed();}
    public Boolean leftBarVisibility() {return leftNavBar.isDisplayed();}


}

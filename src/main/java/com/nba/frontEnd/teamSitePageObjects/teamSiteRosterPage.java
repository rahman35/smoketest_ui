package com.nba.frontEnd.teamSitePageObjects;

import com.nba.frontEnd.base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.BeforeTest;


public class teamSiteRosterPage extends BasePage {

    @FindBy (xpath = "//header/div[3]/nav[1]") WebElement primaryNav;
    @FindBy (xpath = "//body/div[@id='__next']/main[1]/div[1]/div[2]/div[1]/div[1]/a[1]/div[1]/div[2]/div[1]/img[1]")WebElement firstPlayer;
    @FindBy (xpath = "//div[contains(text(),'Season')]")WebElement season;
    @FindBy (xpath = "//div[contains(text(),'Career')]")WebElement career;
    @FindBy (xpath = "//div[contains(text(),'Height')]")WebElement height;
    @FindBy (xpath = "//div[contains(text(),'Weight')]")WebElement weight;
    @FindBy (xpath = "//div[contains(text(),'Age')]")WebElement age;
    @FindBy (xpath = "//div[contains(text(),'Years Pro')]")WebElement yearsPro;
    @FindBy (xpath = "//div[contains(text(),'Country')]")WebElement country;
    @FindBy (xpath = "//div[contains(text(),'Head Coach')]")WebElement headCoach;
    @FindBy (linkText = "Player Bio")WebElement playersBio;
    @FindBy (linkText = "Shop Player Merch")WebElement shopPlayersMerch;

    public teamSiteRosterPage(WebDriver driver) { super(driver); }
    {
        driver.get(coprDev+"www-qa.nba.com/bulls/roster");
        driver.manage().window().maximize();
        driver.findElement(By.id("username")).sendKeys("nbaDev");
        driver.findElement(By.id("password")).sendKeys("Basketba11");
        driver.findElement(By.xpath("//button[contains(text(),'Login')]")).click();
       // driver.findElement(By.id("onetrust-accept-btn-handler")).click();
    }

    public Boolean getPrimaryNav() {return primaryNav.isDisplayed();}
    public Boolean getFirstPLayer() {return firstPlayer.isDisplayed();}
    public boolean clickFirstPlayer() {firstPlayer.click();return true; }
    public Boolean seasonVisibility() {return season.isDisplayed();}
    public Boolean careerVisibility() {return career.isDisplayed();}
    public Boolean heightVisibility() {return height.isDisplayed();}
    public Boolean weightVisibility() {return weight.isDisplayed();}
    public Boolean ageVisibility() {return age.isDisplayed();}
    public Boolean yearsProVisibility() {return yearsPro.isDisplayed();}
    public Boolean countryVisibility() {return country.isDisplayed();}
    public Boolean headCoachVisibility() {return headCoach.isDisplayed();}


    public teamSitePlayersStatsPage playersBioClick()
    {
        playersBio.click();
        return new teamSitePlayersStatsPage(driver);
    }

    public teamSitePlayersMerchandisePage shopPlayersMerchClick()
    {
        shopPlayersMerch.click();
        return new teamSitePlayersMerchandisePage(driver);
    }

}

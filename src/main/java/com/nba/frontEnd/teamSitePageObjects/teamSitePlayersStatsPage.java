package com.nba.frontEnd.teamSitePageObjects;

import com.nba.frontEnd.base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class teamSitePlayersStatsPage extends BasePage {

    @FindBy (className = "player-img") WebElement playersHeadShot;
    @FindBy (xpath = "//div[contains(text(),'Season Type')]")WebElement seasonType;
    @FindBy (xpath = "//div[contains(text(),'Per Mode')]")WebElement perMode;



    public teamSitePlayersStatsPage(WebDriver driver) {
        super(driver);
    }

    public Boolean headshotVisibility(){return playersHeadShot.isDisplayed();}
    public Boolean seasonTypeVisibility(){return seasonType.isDisplayed(); }
    public Boolean perModeVisibility(){return perMode.isDisplayed();}

}
